package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefintions {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor berto = Actor.named("Berto");
	
	@Before
	public void configuracionInicial()
	{
		//le damos la habilidad de navegar en la web a el actor Berto
		berto.can(BrowseTheWeb.with(hisBrowser)); 
		
	}
	@Dado("^que Berto quiere usar el Traductor de Google$")
	public void queBertoQuiereUsarElTraductorDeGoogle() throws Exception {
		berto.wasAbleTo(Abrir.LaPaginaDeGoogle());
    
    
	}


	@Cuando("^el traduce la palabra (.*) de Ingles a Espanol$")
	public void elTraduceLaPalabraTableDeInglesAEspanol(String palabra)  {
		berto.attemptsTo(Traducir.DeInglesAEspanolLa(palabra));
		
	}

	@Entonces("^el deberia ver la palabra (.*) en la pantalla$")
	public void elDeberiaVerLaPalabraMesaEnLaPantalla(String palabraEsperada) {
		berto.should(seeThat(LaRespuesta.es(), equalTo(palabraEsperada)));
    
	}
	


}
