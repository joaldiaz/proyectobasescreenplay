package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.ElNombre;
import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.questions.Respuesta;
import co.com.proyectobase.screenplay.tasks.AbrirPagina;
import co.com.proyectobase.screenplay.tasks.AbrirWebOrange;
import co.com.proyectobase.screenplay.tasks.BuscarEmpleado;
import co.com.proyectobase.screenplay.tasks.Registrar;
import co.com.proyectobase.screenplay.tasks.RegistrarEmpleado;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class WebAutoOrangeStepDefinitions {

	private String primerNombre;
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial()
	{
		//le damos la habilidad de navegar en la web a el actor Berto
		juan.can(BrowseTheWeb.with(hisBrowser)); 
		
	}
		
	@Dado("^que Juan necesita crear un empleado en el OrageHR$")
	public void queJuanNecesitaCrearUnEmpleadoEnElOrageHR() {
		juan.wasAbleTo(AbrirWebOrange.LaPaginaWebOrange());
	    
	}


	@Cuando("^el realiza el ingreso del registro en la aplicación$")
	public void elRealizaElIngresoDelRegistroEnLaAplicación(DataTable dtDatosForm) throws Exception {
		List<List<String>> data = dtDatosForm.raw();
		String nombreCompleto = "";
		for(int i=0; i<data.size(); i++) {
			juan.attemptsTo(RegistrarEmpleado.UsuariosEnFormulario(data, i));
			
			primerNombre = data.get(i).get(0).trim();
			nombreCompleto = data.get(i).get(0).trim()+" "+data.get(i).get(1).trim()+" "+data.get(i).get(2).trim();
			
		}
		
		Serenity.setSessionVariable("nombreEmpleado").to(nombreCompleto);
	    		
	}

	@Entonces("^el visualiza el nuevo empleado en el aplicativo$")
	public void elVisualizaElNuevoEmpleadoEnElAplicativo() throws Exception {
		
		String nombreCompleto = Serenity.sessionVariableCalled("nombreEmpleado").toString();
		juan.attemptsTo(BuscarEmpleado.buscarEmpleado(primerNombre));
		juan.should(seeThat(ElNombre.delEmpleado(), hasItem(nombreCompleto)));
			
				
				
	
	}
		    
	   
}
