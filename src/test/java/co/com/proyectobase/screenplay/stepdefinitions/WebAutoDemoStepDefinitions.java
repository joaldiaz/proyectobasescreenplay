package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.questions.Respuesta;
import co.com.proyectobase.screenplay.tasks.AbrirPagina;
import co.com.proyectobase.screenplay.tasks.Registrar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class WebAutoDemoStepDefinitions {
	
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial()
	{
		//le damos la habilidad de navegar en la web a el actor Berto
		carlos.can(BrowseTheWeb.with(hisBrowser)); 
		
	}
	
	
	@Dado("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite() throws Exception {
	    carlos.wasAbleTo(AbrirPagina.LaPaginaWebDemo());
	    
	}


	@Cuando("^el realiza el registro en la página$")
	public void elRealizaElRegistroEnLaPágina(DataTable dtDatosForm) throws Exception {
			List<List<String>> data = dtDatosForm.raw();
		 
			for(int i=0; i<data.size(); i++) {
				carlos.attemptsTo(Registrar.UsuariosEnFormulario(data, i));
				
			}
		    
	}

	@Entonces("^el verifica que se carga la pantalla con texto (.*)$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow(String palabra) {
		
		//carlos.should(GivenWhenThen.seeThat(Respuesta.texto(), Matchers.equalTo(palabra)));
	  
	}

}
