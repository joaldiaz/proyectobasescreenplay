#Author: jadiaz@choucairtesting.com
# language:es
Característica: Traductor Google
	Como usuario
	Quiero ingresar al traductor de Google
	A traducir palabras entre diferentes lenguajes.

Escenario: Traducir de Ingles a Espanol
	Dado que Berto quiere usar el Traductor de Google
	Cuando el traduce la palabra table de Ingles a Espanol
	Entonces el deberia ver la palabra mesa en la pantalla
  
