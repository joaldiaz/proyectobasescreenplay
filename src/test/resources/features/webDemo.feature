#Author: jadiaz@choucairtesting.com

# language:es
Característica: Web Demo
	Como usuario
	Quiero ingresar al Web Automation Demo Site
	Para registrarme en la pagina.

@CasoExitoso
Esquema del escenario: Registrar en la pagina

	Dado que Carlos quiere acceder a la Web Automation Demo Site
	Cuando el realiza el registro en la página
	|<Primernombre>|<apellido>|<Direccion>|<Correoelectronico>|<telefono>|<genero>|<hobbies>|<lenguajes>|<skills>|<Pais>|<selectpais>|<año>|<mes>|<dia>|<passowrd>|<confirmarpassword>|
	Entonces el verifica que se carga la pantalla con texto Double Click on Edit Icon to EDIT the Table Row
Ejemplos:
|Primernombre |apellido|Direccion|Correoelectronico|telefono|genero|hobbies|lenguajes|skills|Pais|selectpais|año|mes|dia|passowrd|confirmarpassword|

