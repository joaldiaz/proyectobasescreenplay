#Author: jadiaz@choucairtesting.com
# language:es
Característica: Web Demo
  Como usuario
  Quiero ingresar al Web Automation Demo Site
  Para registrarme en la pagina.

  @CasoExitoso
  Esquema del escenario: Registrar en la pagina
    Dado que Juan necesita crear un empleado en el OrageHR
    Cuando el realiza el ingreso del registro en la aplicación
      | <Nombre> | <Primerapellido> | <Segundoapellido> | <IdEmpleado> | <Locacion> | <EstadoMarital> | <Genero> | <Nacionalidad> | <GrupoSanguineo> | <Hobbies> |
    Entonces el visualiza el nuevo empleado en el aplicativo

    Ejemplos: 
      | Nombre     | Primerapellido | Segundoapellido | IdEmpleado | Locacion               | EstadoMarital | Genero | Nacionalidad | GrupoSanguineo | Hobbies       | 
      | John Alexander | Diaz        | Ramirez         | 1059784412 | Australian Regional HQ | Single        | Males  | Ukrainian    | AB              | Playing Chess |
