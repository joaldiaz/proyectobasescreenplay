package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.WebAutoDemoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirPagina implements Task{

	private WebAutoDemoPage webAutoDemoPage;
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webAutoDemoPage));
		
	}

	public static AbrirPagina LaPaginaWebDemo() {
		
		return Tasks.instrumented(AbrirPagina.class);
	}

}
