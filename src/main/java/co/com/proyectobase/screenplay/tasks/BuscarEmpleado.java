package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.ui.BuscarEmpleadoOrangePage;
import co.com.proyectobase.screenplay.ui.WebAutoDemoPage;
import co.com.proyectobase.screenplay.ui.WebAutoOrangePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.ClickOnTarget;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class BuscarEmpleado implements Task {
	
	private String nombre;
	
	
	public BuscarEmpleado(String nombre) {
		this.nombre = nombre;
	}


	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(BuscarEmpleadoOrangePage.OPCION_LIST_EMPLOYEE));
		actor.attemptsTo(Esperar.aMoment(3000));
		actor.attemptsTo(Click.on(BuscarEmpleadoOrangePage.CAMPO_EMPLOYEE_NAME));
		actor.attemptsTo(Enter.theValue(nombre).into(BuscarEmpleadoOrangePage.CAMPO_EMPLOYEE_NAME));
		actor.attemptsTo(Click.on(BuscarEmpleadoOrangePage.BUSQUEDA_EMPLOYEE_NAME));
		actor.attemptsTo(Esperar.aMoment(4000));
		
		
	}


	public static BuscarEmpleado buscarEmpleado(String nombre) {
		
		return Tasks.instrumented(BuscarEmpleado.class,nombre);
	}

}
