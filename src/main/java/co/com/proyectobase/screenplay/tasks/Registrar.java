package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.apache.tools.ant.taskdefs.Sleep;

import co.com.proyectobase.screenplay.ui.WebAutoDemoPage;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Registrar implements Task{
	
	private List<List<String>> data;
	private int i; 
	public Registrar(List<List<String>> data,int i) {
		super();
		this.data = data;
		this.i=i;
	}


	@Override
	public <T extends Actor> void performAs(T actor) {
		
		
		actor.attemptsTo(Click.on(WebAutoDemoPage.CAMPO_PRIMER_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(WebAutoDemoPage.CAMPO_PRIMER_NOMBRE));
		actor.attemptsTo(Click.on(WebAutoDemoPage.CAMPO_APELLIDO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(WebAutoDemoPage.CAMPO_APELLIDO));
		actor.attemptsTo(Click.on(WebAutoDemoPage.CAMPO_DIRECCION));
		actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(WebAutoDemoPage.CAMPO_DIRECCION));
		actor.attemptsTo(Click.on(WebAutoDemoPage.CAMPO_CORREO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(3).trim()).into(WebAutoDemoPage.CAMPO_CORREO));
		actor.attemptsTo(Click.on(WebAutoDemoPage.CAMPO_TELEFONO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(4).trim()).into(WebAutoDemoPage.CAMPO_TELEFONO));
		actor.attemptsTo(Click.on(WebAutoDemoPage.RADIO_OPCION_GENERO));
		actor.attemptsTo(Click.on(WebAutoDemoPage.CHECKBOX_HOBBIES));
		actor.attemptsTo(Click.on(WebAutoDemoPage.CAMPO_LENGUAJES));
		actor.attemptsTo(Click.on(WebAutoDemoPage.OPCION_LENGUAJE));
		actor.attemptsTo(Click.on(WebAutoDemoPage.LABEL_LENGUAJE));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(i).get(9)).from(WebAutoDemoPage.CAMPO_PAIS));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(i).get(11)).from(WebAutoDemoPage.CAMPO_AÑO));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(i).get(12)).from(WebAutoDemoPage.CAMPO_MES));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(i).get(13)).from(WebAutoDemoPage.CAMPO_DIA));
		actor.attemptsTo(Enter.theValue(data.get(i).get(14).trim()).into(WebAutoDemoPage.CAMPO_CLAVE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(15).trim()).into(WebAutoDemoPage.CAMPO_CONFIRMAR_CLAVE));
		actor.attemptsTo(Click.on(WebAutoDemoPage.BOTON_REGISTRAR));
		
		
	}

	public static Registrar UsuariosEnFormulario(List<List<String>> data,int i) {
				
			
		return Tasks.instrumented(Registrar.class,data,i);
	}
	
	
	

}
