package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.ui.WebAutoDemoPage;
import co.com.proyectobase.screenplay.ui.WebAutoOrangePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.ClickOnTarget;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import java.lang.Thread;

public class RegistrarEmpleado  implements Task{
	
	
	private List<List<String>> data;
	private int i; 
	
	public RegistrarEmpleado(List<List<String>> data,int i) {
		super();
		this.data = data;
		this.i=i;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_PRIMER_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(WebAutoOrangePage.CAMPO_PRIMER_NOMBRE));
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_PRIMER_APELLIDO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(WebAutoOrangePage.CAMPO_PRIMER_APELLIDO));
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_SEGUNDO_APELLIDO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(WebAutoOrangePage.CAMPO_SEGUNDO_APELLIDO));
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_ID_EMPLEADO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(3).trim()).into(WebAutoOrangePage.CAMPO_ID_EMPLEADO));
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_LOCACION));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(4)+ "')]")); 
		
		actor.attemptsTo(Click.on(WebAutoOrangePage.BOTON_SAVE1));
		
		actor.attemptsTo(Esperar.aMoment(5000));
		
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_ESTADO_CIVIL));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(5)+ "')]")); 
		
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_NACIONALIDAD));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(7)+ "')]"));
		actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_GRUPO_SANGUINEO));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(8)+ "')]")); 
		actor.attemptsTo(Enter.theValue(data.get(i).get(9).trim()).into(WebAutoOrangePage.CAMPO_HOBBIES));
		
		actor.attemptsTo(Click.on(WebAutoOrangePage.BOTON_SAVE));
		
		actor.attemptsTo(Esperar.aMoment(5000));
		
		
		
		
	}

	public static RegistrarEmpleado UsuariosEnFormulario(List<List<String>> data, int i) {
		
		return Tasks.instrumented(RegistrarEmpleado.class,data,i);
	}
	

}
