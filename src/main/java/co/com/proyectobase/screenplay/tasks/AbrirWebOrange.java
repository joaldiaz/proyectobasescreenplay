package co.com.proyectobase.screenplay.tasks;
import java.lang.Thread;

import co.com.proyectobase.screenplay.ui.WebAutoOrangePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirWebOrange implements Task{
	
	private WebAutoOrangePage webAutoOrangePage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webAutoOrangePage));
		actor.attemptsTo(Click.on(WebAutoOrangePage.BOTON_LOGIN));
		actor.attemptsTo(Click.on(WebAutoOrangePage.OPCION_PIM));
		actor.attemptsTo(Click.on(WebAutoOrangePage.OPCION_EMPLOYEE));
		
		
		
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		//actor.attemptsTo(Click.on(WebAutoOrangePage.CAMPO_PRIMER_NOMBRE));
		
		
	}

	public static AbrirWebOrange LaPaginaWebOrange() {
		
		return Tasks.instrumented(AbrirWebOrange.class);
	}

}
