package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class WebAutoOrangePage extends PageObject {
	
	public static final Target BOTON_LOGIN = Target.the("Boton dodne se loguea el administrador").located(By.id("btnLogin"));
	public static final Target OPCION_PIM = Target.the("opcion para desplegar opcion add employee").located(By.id("menu_pim_viewPimModule"));
	public static final Target OPCION_EMPLOYEE = Target.the("opcion para adicionar empleado").located(By.id("menu_pim_addEmployee"));
	public static final Target CAMPO_PRIMER_NOMBRE = Target.the("Campo para  escribir el nombre del empleado").located(By.id("firstName"));
	public static final Target CAMPO_PRIMER_APELLIDO = Target.the("Campo para  escribir el primer apellido del empleado").located(By.id("middleName"));
	public static final Target CAMPO_SEGUNDO_APELLIDO = Target.the("Campo para  escribir el segundo apellido del empleado").located(By.id("lastName"));
	public static final Target CAMPO_ID_EMPLEADO = Target.the("Campo para  escribir el id del empleado").located(By.id("employeeId"));
	public static final Target CAMPO_LOCACION = Target.the("Campo para  escribir el la locacion del empleado").located(By.id("location_inputfileddiv"));
	
	public static final Target CAMPO_LOCACION2 = Target.the("Campo para  escribir el la locacion del empleado").located(By.id("select-options-1e6aabf0-1ec8-1ab9-3c68-b5a2752ad69e"));
	

	public static final Target BOTON_SAVE1 = Target.the("boton para guardar los cambios   del empleado").located(By.xpath("//A[@id='systemUserSaveBtn']"));
	public static final Target CAMPO_ESTADO_CIVIL = Target.the("Campo para  escribir el estado civil del empleado").located(By.id("marital_status_inputfileddiv"));
	public static final Target RADIO_GENERO = Target.the("Campo para  seleccionar el genero del empleado").located(By.id("marital_status_inputfileddiv"));
	
	public static final Target CAMPO_NACIONALIDAD = Target.the("Campo para  seleccionar la nacionalidad del empleado").located(By.id("nationality_inputfileddiv"));
	public static final Target CAMPO_RAZA = Target.the("Campo para  seleccionar la nacionalidad del empleado").located(By.id("eeo_race_ethnicity_inputfileddiv"));
	public static final Target CAMPO_GRUPO_SANGUINEO = Target.the("Campo para  seleccionar el grupo sanguieo  del empleado").located(By.id("1_inputfileddiv"));
	public static final Target CAMPO_HOBBIES = Target.the("Campo para  el hobbie  del empleado").located(By.id("5"));
	public static final Target BOTON_SAVE = Target.the("boton para guardar los cambios   del empleado").located(By.xpath("//*[@id=\"content\"]/div[2]/ui-view/div[2]/div/custom-fields-panel/div/form/materializecss-decorator[3]/div/sf-decorator/div/button"));
	public static final Target BOTON_SAVE2 = Target.the("boton para guardar los cambios   del empleado").located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[8]/div/sf-decorator[2]/div/button"));
	public static final Target OPCION_LIST_EMPLOYEE = Target.the("opcion para listar los empleados").located(By.id("menu_pim_viewEmployeeList"));
	//public static final Target OPCION_LIST_EMPLOYEE = Target.the("opcion para listar los empleados").located(By.xpath("//SPAN[@class='left-menu-title'][text()='Employee List']"));
	
	public static final Target CAMPO_EMPLOYEE_NAME = Target.the("opcion para consultar  empleado").located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target BUSQUEDA_EMPLOYEE_NAME = Target.the("opcion para consultar  empleado").located(By.id("quick_search_icon"));
	public static final Target RESULTADO_EMPLOYEE_NAME = Target.the("opcion para consultar  empleado").located(By.id("employeeListTable"));
	public static final Target TABLA_RESULTADO_EMPLEADOS = Target.the("Tabla donde se visualiza el resultado de la busqueda").locatedBy("//td[@ng-click='vm.viewProfileIfHasAccessablePimTabs(employee)'][3]"); 
		
}
