package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;



public class BuscarEmpleadoOrangePage extends PageObject {
	
	
	
	public static final Target OPCION_LIST_EMPLOYEE = Target.the("opcion para listar los empleados").located(By.id("menu_pim_viewEmployeeList"));
	//public static final Target OPCION_LIST_EMPLOYEE = Target.the("opcion para listar los empleados").located(By.xpath("//SPAN[@class='left-menu-title'][text()='Employee List']"));
	
	public static final Target CAMPO_EMPLOYEE_NAME = Target.the("opcion para consultar  empleado").located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target BUSQUEDA_EMPLOYEE_NAME = Target.the("opcion para consultar  empleado").located(By.id("quick_search_icon"));
	public static final Target RESULTADO_EMPLOYEE_NAME = Target.the("opcion para consultar  empleado").located(By.id("employeeListTable"));
	public static final Target TABLA_RESULTADO_EMPLEADOS = Target.the("Tabla donde se visualiza el resultado de la busqueda").locatedBy("//td[@ng-click='vm.viewProfileIfHasAccessablePimTabs(employee)'][3]"); 
}
