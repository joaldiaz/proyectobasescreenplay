package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class GoogleTraductorPage  extends PageObject{
	
	public static final Target BOTON_LENGUAJE_ORIGEN = Target.the("El botón que seleciona el lenguaje origen").located(By.id("gt-sl-gms"));
	public static final Target BOTON_LENGUAJE_DESTINO = Target.the("EL botón que selecciona el lenguaje destino ").located(By.id("gt-tl-gms"));
	//public static final Target OPCION_INGLES = Target.the("Opcion Ingles").located(By.xpath("//div[@id=\'gt-sl-gms-menu\']/table/body/tr/td/div[contains(text(),'Ingl')]"));
	//public static final Target OPCION_ESPANOL = Target.the("Opcion Español").located(By.xpath("//div[@id=\'gt-tl-gms-menu\']/table/body/tr/td/div[contains(text(),'Espa')]"));
	public static final Target OPCION_INGLES = Target.the("Opcion Inglés").located(By.xpath("//*[@id=\'sugg-item-es\']"));
	public static final Target OPCION_ESPANOL = Target.the("Opcion Español").located(By.xpath("//*[@id=\'sugg-item-en\']"));
	public static final Target AREA_DE_TRADUCCION = Target.the("EL lugar donde se escriben las palabras a traducir").located(By.id("source"));
	public static final Target BOTON_TRADUCIR = Target.the("El Botón traducir").located(By.id("gt-submit"));
	public static final Target AREA_TRADUCIDA = Target.the("EL area donde ya se presenta la palabra traducida").located(By.id("gt-res-dir-ctr"));

}
