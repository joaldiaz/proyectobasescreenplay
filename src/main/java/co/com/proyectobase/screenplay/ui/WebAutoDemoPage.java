package co.com.proyectobase.screenplay.ui;

 
import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class WebAutoDemoPage extends PageObject {
	
	public static final Target CAMPO_PRIMER_NOMBRE = Target.the("EL campo donde se escribe el primer nombre").located(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[1]/input"));
	public static final Target CAMPO_APELLIDO = Target.the("Campo donde se escribe el apellido").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[2]/input"));
	public static final  Target CAMPO_DIRECCION = Target.the("Campo donde se escribe la direccion").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[2]/div/textarea"));
	public static final Target CAMPO_CORREO = Target.the("Campo donde se escribe el correo").located(By.xpath("//*[@id=\"eid\"]/input"));
	public static final Target CAMPO_TELEFONO = Target.the("Campo donde se escribe el telefono").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[4]/div/input"));
	public static final Target RADIO_OPCION_GENERO = Target.the("opcion donde se seleccion el genero").located(By.name("radiooptions"));
	public static final Target CHECKBOX_HOBBIES = Target.the("Checkbox donde se selecciona el hobbie").located(By.id("checkbox2"));
	public static final Target CAMPO_LENGUAJES = Target.the("Campo donde se escribe el lenguaje").located(By.id("msdd"));
	public static final Target OPCION_LENGUAJE = Target.the("Campo donde se escoje el lenguaje").located(By.xpath("//A[@class='ui-corner-all'][text()='Japanese']"));
    public static final Target LABEL_LENGUAJE = Target.the("Campo para dar click afuera del campo lenguaje ").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[7]/label"));
	public static final Target CAMPO_HABILIDADES = Target.the("Campo donde se selecciona la habilidad").located(By.id("Skills"));
	public static final Target CAMPO_PAIS = Target.the("Campo para seleccionar pais").located(By.id("countries"));
	public static final Target CAMPO_SELECCION_PAIS = Target.the("Campo para seleccionar pais 2").located(By.xpath("//span[@class='select2-selection select2-selection--single']"));
	public static final Target CAMPO_AÑO = Target.the("Campo para el año").located(By.id("yearbox"));
	public static final Target CAMPO_MES = Target.the("Campo para el mes").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[11]/div[2]/select"));
	public static final Target CAMPO_DIA = Target.the("Campo para el dia").located(By.id("daybox"));
	public static final Target CAMPO_CLAVE = Target.the("Campo para la clave").located(By.id("firstpassword"));
	public static final Target CAMPO_CONFIRMAR_CLAVE = Target.the("Campo para confirmar l a clave").located(By.xpath("//input[@id='secondpassword']"));
	//public static final Target CAMPO_CONFIRMAR_CLAVE = Target.the("Campo para confirmar l a clave").located(By.id("secondpassword"));	
	public static final Target BOTON_REGISTRAR = Target.the("Boton para completar el registro").located(By.id("submitbtn"));
	public static final Target LABEL_EDIT = Target.the("Texto para verificar el registro exitoso").located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
	

}
