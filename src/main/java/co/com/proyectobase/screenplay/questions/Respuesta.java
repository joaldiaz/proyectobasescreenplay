package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.WebAutoDemoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Respuesta implements Question<String>{
	public static Respuesta texto() {
		
		return new Respuesta();
	}
	
	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(WebAutoDemoPage.LABEL_EDIT).viewedBy(actor).asString();
	}
}
