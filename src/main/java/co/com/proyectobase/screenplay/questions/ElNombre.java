package co.com.proyectobase.screenplay.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import co.com.proyectobase.screenplay.ui.BuscarEmpleadoOrangePage;
import co.com.proyectobase.screenplay.ui.WebAutoOrangePage;

public class ElNombre implements Question<List<String>> {
	

	public static Question<List<String>> delEmpleado() {
		return new ElNombre();
	}
	
	@Override
	public List<String> answeredBy(Actor actor) {
		
		return Text.of(BuscarEmpleadoOrangePage.TABLA_RESULTADO_EMPLEADOS)                  
	            .viewedBy(actor)                        
	            .asList();
	}

	
	/*public String recorrerDatosTabla() {
		//WebElement table = getDriver().findElement(By.tagName("table"));
		List<WebElement> tabla = (List<WebElement>) OrangeListaEmpleadosPage.TABLA_RESULTADO_EMPLEADOS;
		List<WebElement> allRows = ((WebElement) tabla).findElements(By.tagName("tr"));
		for (int i = 0; i < allRows.size(); i++) {
			List<WebElement> cells = allRows.get(i).findElements(By.tagName("td"));
			String cell = cells.get(2).getText();
			
		}
		
	}*/
}